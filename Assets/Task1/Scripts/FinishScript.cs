using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class FinishScript : MonoBehaviour
{
    public UnityEvent NextLevelEvent;

    private Coroutine coroutine;
    private Text CountdownText;
    public GameObject FinishCountdown;
    public GameObject FinishCountdownText;

    private string initialCountdownText;
    private string countdownText;

    private void Start()
    {
        CountdownText = FinishCountdownText.GetComponent<Text>();
        initialCountdownText = CountdownText.text;
    }
    //��������� � ������� �������� ���� � ��������� �������
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            countdownText = initialCountdownText;
            FinishCountdown.SetActive(true);
            coroutine = StartCoroutine(Timer());

            Debug.Log("Entered");
        }
    }
    //����� �� �������� �������� ���� � ����� �������
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            StopCoroutine(coroutine);
            FinishCountdown.SetActive(false);

            Debug.Log("Exited");
        }
    }
    private IEnumerator Timer()
    {

        for (int i = 3; i >= 0; i--)
        {
            yield return new WaitForSeconds(1);
            countdownText = i.ToString();
            CountdownText.text = countdownText;

            Debug.Log(i);
        }
        //����� ��� ����� ������ �� ������
        NextLevelEvent.Invoke();
    }
}
