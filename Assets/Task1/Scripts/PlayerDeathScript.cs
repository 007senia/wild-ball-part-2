using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeathScript : MonoBehaviour
{
    public ParticleSystem PS;

    private Vector3 DeathPosition;
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Spikes"))
        {
            DeathPosition = gameObject.transform.position;
            Instantiate(PS, DeathPosition, Quaternion.Euler(-90, 0, 0));
        }
    }
}
