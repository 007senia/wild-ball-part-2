using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WildBall.Inputs
{
    [RequireComponent(typeof(PlayerMovement))]
    public class PlayerInput : MonoBehaviour
    {
        private Vector3 movement;
        private PlayerMovement playerMovement;

        private void Awake()
        {
            playerMovement = GetComponent<PlayerMovement>();
        }
        void Update()
        {
            Inputs();
        }

        private void FixedUpdate()
        {
            playerMovement.MoveCharacter(movement);
        }

        public void Inputs()
        {
            float horizontal = Input.GetAxis(GlobalStringVars.HORIZONTAL_AXIS);
            float vertical = Input.GetAxis(GlobalStringVars.VERTICAL_AXIS);

            //������ ���������� ���� ��� ������ ����������� ������� ������
            if (horizontal == 0 && vertical != 0)
            {
                movement = new Vector3(-playerMovement.playerRigidbody.velocity.x, 0, vertical).normalized;
            }
            else if (horizontal != 0 && vertical == 0)
            {
                movement = new Vector3(horizontal, 0, -playerMovement.playerRigidbody.velocity.z).normalized;
            }
            else if (horizontal == 0 && vertical == 0)
            {
                movement = new Vector3(-playerMovement.playerRigidbody.velocity.x, 0, -playerMovement.playerRigidbody.velocity.z).normalized;
            }
            else
            {
                movement = new Vector3(horizontal, 0, vertical).normalized;
            }
        }
    }
}
