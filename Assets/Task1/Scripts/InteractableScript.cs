using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WildBall.Inputs;

public class InteractableScript : MonoBehaviour
{
    public GameObject ToolTip;
    public GameObject Door1;
    public GameObject Door2;
    private float doorTime = 0.0f;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ToolTip.SetActive(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ToolTip.SetActive(false);
        }
    }
    private void OpenDoor()
    {
        float Interact = Input.GetAxis(GlobalStringVars.INTERACT_BUTTON);

        if (Interact != 0 && doorTime <= 0)
        {
            if (Door1.activeSelf)
            {
                Door1.SetActive(false);
                Door2.SetActive(true);
                doorTime = 1.0f;
            }
            else
            {
                Door1.SetActive(true);
                Door2.SetActive(false);
                doorTime = 1.0f;
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            OpenDoor();
            Timer();
        }
    }
    //��� ����, ����� �������� ������� �������� ������ �� ��������� ����� ������ �����,
    //��� ������ ������, �� ����������� �������� �� ������ ������� ������.
    private void Timer()
    {
        if (doorTime > 0)
        {
            doorTime -= Time.deltaTime;
        }
        Debug.Log(doorTime.ToString());
    }
}
