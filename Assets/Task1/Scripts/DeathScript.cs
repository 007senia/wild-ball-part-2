using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathScript : MonoBehaviour
{
    public GameObject FailUI;
    private void OnCollisionEnter(Collision collision)
    {
        collision.gameObject.SetActive(false);
        FailUI.SetActive(true);
    }
}
